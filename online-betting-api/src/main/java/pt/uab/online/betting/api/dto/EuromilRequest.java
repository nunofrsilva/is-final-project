package pt.uab.online.betting.api.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class EuromilRequest {

    @NotBlank
    private String key;

    @Pattern(regexp = "^[0-9]{16}$", flags = Pattern.Flag.CASE_INSENSITIVE, message = "checkid invalido, verifique se tem 16 dígitos.")
    private String checkid;
}
