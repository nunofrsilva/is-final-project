package pt.uab.online.betting.api.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DigitalCheck {
    private LocalDateTime date;
    private String checkID;
}
