package pt.uab.online.betting.api.client;

import euromil.EuromilGrpc;
import euromil.EuromilOuterClass;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Cliente utilizado para comunicar com o serviço EuroMilRegister
 */
@Component
public class EuromilClient {

    //gRPC Host
    @Value("${euromil.grpc.host}")
    private String euromilGrpcHost;
    // gRPC port
    @Value("${euromil.grpc.port}")
    private int euromilGrpcPort;

    /**
     * Metodo utilizado para comunicar com o serviço EuroMilRegister
     * @param key
     * @param checkId
     * @return
     */
    public String register(final String key, final String checkId) {
        // Cria um gRPC channel para comunicação com o servidor
        ManagedChannel channel = ManagedChannelBuilder.forAddress(euromilGrpcHost, euromilGrpcPort)
                .usePlaintext()
                .build();
        EuromilGrpc.EuromilBlockingStub stub = EuromilGrpc.newBlockingStub(channel);
        // Invoca o metodo RegisterEuroMil, utilizando o EuromilOuterClass.RegisterRequest definido no euromil.proto
        EuromilOuterClass.RegisterReply response = stub.registerEuroMil(EuromilOuterClass.RegisterRequest.newBuilder()
                .setKey(key)
                .setCheckid(checkId)
                .build()
        );
        // Fecha o canal
        channel.shutdown();
        // Retorna mensagem de sucesso ou erro, obtida do serviço EuroMilRegister
        return response.getMessage();
    }
}
