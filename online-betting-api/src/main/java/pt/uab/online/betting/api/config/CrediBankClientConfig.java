package pt.uab.online.betting.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Configuração do webclient a ser utilizado pelo cliente CrediBanckClient
 */
@Configuration
public class CrediBankClientConfig {

    @Value("${credibank.service.url}")
    private String credibankServiceUrl;

    @Bean(name = "credibankWebClient")
    WebClient webClient() {
        return WebClient.builder().baseUrl(credibankServiceUrl).build();
    }
}
