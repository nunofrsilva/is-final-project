package pt.uab.online.betting.api.rest;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pt.uab.online.betting.api.client.EuromilClient;
import pt.uab.online.betting.api.dto.EuromilRequest;

import javax.validation.Valid;

@RestController
@Tag(name = "Sistema EuroMilRegister", description = "Permite registar apostas online no jogo Euromilhões.")
public class EuromilController {

    @Autowired
    private EuromilClient euromilClient;

    @PostMapping("/euromil")
    private String register(@Valid @RequestBody EuromilRequest request) {
        return euromilClient.register(request.getKey(), request.getCheckid());
    }
}
