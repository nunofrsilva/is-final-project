package pt.uab.online.betting.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineBettingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineBettingApiApplication.class, args);
	}

}
