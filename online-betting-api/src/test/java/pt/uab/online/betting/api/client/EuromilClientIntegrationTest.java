package pt.uab.online.betting.api.client;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class EuromilClientIntegrationTest {

    @Autowired
    public EuromilClient client;

    @Test
    public void register_thanReturnErrorMessage() {
        final String msg = client.register("21","1234567890123456");
        assertEquals("Error. Key is not Valid.", msg);
    }

    @Test
    public void register_thanReturnSuccessMessage() {
        final String key = "1 2 3 4 5 6 7";
        final String msg = client.register(key,"1234567890123456");
        assertTrue(msg.contains("Success. Key ("+key+") is registered with CheckID"));
        //Success. Key (1 2 3 4 5 6 7) is registered with CheckID
    }
}
