package pt.uab.apibetregister.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import pt.uab.apibetregister.dto.CrediBankRequest;
import pt.uab.apibetregister.dto.CrediBankResponse;
import pt.uab.apibetregister.dto.EuromilRequest;
import reactor.core.publisher.Mono;

@Component
public class OnlineBettingClient {
    private static String CREDBANK = "/credibank";
    private static String EUROMIL = "/euromil";

    @Autowired
    @Qualifier("onlineBettingWebClient")
    private WebClient webClient;


    public CrediBankResponse issueDigitalCheck(final CrediBankRequest request) {
        return webClient
                .post()
                .uri(CREDBANK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(request), CrediBankRequest.class)
                .retrieve()
                .bodyToMono(CrediBankResponse.class)
                .block();
    }

    public String euromilBetRegister(final EuromilRequest request) {
        return webClient
                .post()
                .uri(EUROMIL)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(request), EuromilRequest.class)
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }
}
