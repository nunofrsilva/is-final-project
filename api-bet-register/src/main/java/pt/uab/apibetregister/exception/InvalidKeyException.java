package pt.uab.apibetregister.exception;

public class InvalidKeyException extends Exception{
    public InvalidKeyException(String message) {
        super(message);
    }
}
