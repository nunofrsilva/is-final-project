package pt.uab.apibetregister.exception;

public class InvalidAccountId extends Exception{
    public InvalidAccountId(String message) {
        super(message);
    }
}
