package pt.uab.apibetregister.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.uab.apibetregister.client.OnlineBettingClient;
import pt.uab.apibetregister.dto.*;

import java.util.stream.Collectors;

@Service
public class BetRegisterService {

    @Autowired
    private OnlineBettingClient client;

    public ServiceResponse register(ServiceRequest request) {
        CrediBankResponse crediBankResponse = issueDigitalCheck(request.getCrediBankAccount());
        return new ServiceResponse(
                crediBankResponse,
                euromilRegister(
                        request.getKey().stream().map(String::valueOf).collect(Collectors.joining(" ","","")),
                        crediBankResponse.getCheckID())
        );
    }

    /**
     * Generate digital check with 10 credits.
     * The credit is fixed because it's a requirement of the task statement
     * @param accountId
     * @return
     */
    private CrediBankResponse issueDigitalCheck(final int accountId) {
        return client.issueDigitalCheck(new CrediBankRequest(
                accountId,
            10.00
        ));
    }

    /**
     * Register bet on EuroMil Register Service
     * @param key
     * @param checkId
     * @return
     */
    private String euromilRegister(final String key, final String checkId) {
        return client.euromilBetRegister(new EuromilRequest(
                key,
                checkId
        ));
    }

}
