package pt.uab.apibetregister.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class OnlineBettingClientConfig {
    @Value("${onlineBetting.service.url}")
    private String onlineBettingServiceUrl;

    @Bean(name = "onlineBettingWebClient")
    WebClient webClient() {
        return WebClient.builder().baseUrl(onlineBettingServiceUrl).build();
    }
}
