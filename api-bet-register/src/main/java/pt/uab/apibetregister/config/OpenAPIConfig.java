package pt.uab.apibetregister.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfig {

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("EuroMil API")
                        .description("This API is used to support EuroMil App")
                        .version("v1.0.0")
                        .contact(new Contact()
                                .name("Nuno Silva")
                                .email("2001499@estudante.uab.pt"))
        );
    }
}
