package pt.uab.apibetregister.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pt.uab.apibetregister.dto.ServiceRequest;
import pt.uab.apibetregister.dto.ServiceResponse;
import pt.uab.apibetregister.exception.InvalidAccountId;
import pt.uab.apibetregister.exception.InvalidKeyException;
import pt.uab.apibetregister.service.BetRegisterService;

import javax.validation.Valid;

@RestController
public class BetRegisterController {

    @Autowired
    private BetRegisterService service;

    @PostMapping("/betting-register")
    private ServiceResponse register(@Valid @RequestBody ServiceRequest request) throws InvalidKeyException, InvalidAccountId {
        request.isValid();
        return service.register(request);
    }
}
