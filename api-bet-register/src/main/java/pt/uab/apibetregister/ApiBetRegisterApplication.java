package pt.uab.apibetregister;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiBetRegisterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiBetRegisterApplication.class, args);
	}
}
