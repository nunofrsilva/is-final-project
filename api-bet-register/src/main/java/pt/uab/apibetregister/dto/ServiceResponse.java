package pt.uab.apibetregister.dto;

import lombok.Data;

@Data
public class ServiceResponse {

    private CrediBankResponse crediBank;
    private String euromil;

    public ServiceResponse(CrediBankResponse crediBank, String euromil) {
        this.crediBank = crediBank;
        this.euromil = euromil;
    }
}
