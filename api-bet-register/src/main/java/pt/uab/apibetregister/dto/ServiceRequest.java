package pt.uab.apibetregister.dto;

import lombok.Data;
import pt.uab.apibetregister.exception.InvalidAccountId;
import pt.uab.apibetregister.exception.InvalidKeyException;

import javax.validation.constraints.Pattern;
import java.util.Set;

@Data
public class ServiceRequest {

    private Set<Integer> key;

    private int crediBankAccount;

    public void isValid() throws InvalidKeyException, InvalidAccountId {
        if (key.isEmpty() || key.stream().anyMatch(n -> n > 50)) {
            throw new InvalidKeyException("Chave inválida!");
        }
        if (String.valueOf(crediBankAccount).length() != 8) {
            throw new InvalidAccountId("Número de conta inválido, verifique se tem 8 dígitos.");
        }
    }
}
