package pt.uab.apibetregister.dto;

import lombok.Data;

@Data
public class EuromilRequest {
    private String key;
    private String checkid;

    public EuromilRequest(String key, String checkId) {
        this.key = key;
        this.checkid = checkId;
    }
}
