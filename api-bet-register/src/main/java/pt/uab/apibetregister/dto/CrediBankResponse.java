package pt.uab.apibetregister.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CrediBankResponse {
    private LocalDateTime date;
    private String checkID;
}
