package pt.uab.apibetregister.dto;

import lombok.Data;

@Data
public class CrediBankRequest {
    private int accountId;
    private Double value;

    public CrediBankRequest(int accountId, Double value) {
        this.accountId = accountId;
        this.value = value;
    }
}
