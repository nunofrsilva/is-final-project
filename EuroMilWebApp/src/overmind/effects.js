import httpClient from "axios";
import SETTINGS from "../config/settings";

const apiHttpClient = httpClient.create({baseURL: SETTINGS.API_URL});
export const api = {
    register(data) {
        return apiHttpClient.post("/betting-register",data);
    },

    getDestinationList(userType = "anonymous") {
        return apiHttpClient.get("/destinations?userType="+userType);
    },

    searchDestination(keyword, userType = "default") {
        return apiHttpClient.get("/destinations/search?pattern="+keyword+"&userType="+userType)
    },
    getDestination(id) {
        return apiHttpClient.get("/destinations/"+id);
    }
};