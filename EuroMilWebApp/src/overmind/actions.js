export const changeNumbers = ({state}, numbers) => {
    state.numbers = [...numbers];
}

export const changeAccount = ({state}, account) => {
    state.account = account;
}

export const setErrorMessage = ({state}, message) => {
    state.errorMessage = message;
}

export const betRegister = async ({state, effects}) => {
    try {
        const {data: {...data}} = await effects.api.register({key: state.numbers, crediBankAccount: state.account})
        state.serverResponse = {...data};
        state.numbers = ['','','','','','',''];
        state.account = '';
    } catch ({message}) {
        console.log("Error to bet register");
    }
}
/*
export const changeUserType = async ({state, effects}, userType) => {
    state.userType = userType;
    state.inputSearch = "";
    try {
        const {data: {data: destinationList = []}} = await effects.api.getDestinationList(userType);
        state.destinationList = [...destinationList];
    } catch ({message}) {
        console.error("Error retrieving destination list", message);
    }
};

export const getDestination = async ({state, effects}, id) => {
    try {
        const {data: {data: destination = {}}} = await effects.api.getDestination(id);
        state.selectedDestination = {...destination};
    } catch ({message}) {
        console.error("Error retrieving destination list", message);
    }
};


*/