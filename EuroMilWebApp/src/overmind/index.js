import { state } from './state'
import * as actions from './actions'
import * as effects from './effects'
import {createActionsHook, createEffectsHook, createHook, createStateHook} from "overmind-react";

export const config = {
    state,
    actions,
    effects
}

export const useState = createStateHook();
export const useActions = createActionsHook();
export const useEffects = createEffectsHook();
export const useApp = createHook();