import React from "react";
import {Route} from 'react-router-dom'
import './App.css';
import Header from "./components/Header";
import HomeContent from "./components/HomeContent"
import {createTheme, ThemeProvider} from "@material-ui/core";
import BetForm from "./components/BetForm";
import FinishContent from "./components/FinishContent";

const theme = createTheme({
    palette: {
        primary: {
            // Purple and green play nicely together.
            main: '#1976d2',
            light: '#63a4ff',
            dark: '#004ba0',
            contrastText: '#ffffff'
        },
        secondary: {
            // This is green.A700 as hex.
            main: '#64b5f6',
            light: '#9be7ff',
            dark: '#2286c3',
            contrastText: '#ffffff'
        },
    },
});


const App = () => {
    return (
        <ThemeProvider theme={theme}>
            <div className="App">
                <Route exact path={"/"} render={() => (
                    <div>
                        <Header />
                        <HomeContent />
                    </div>
                )} />

                <Route exact path={"/bet"} render={() => (
                    <div>
                        <Header />
                        <BetForm />
                    </div>
                )} />
                <Route exact path={"/finish"} render={() => (
                    <div>
                        <Header />
                        <FinishContent />
                    </div>
                )} />
            </div>
        </ThemeProvider>
    );
}

export default App;
