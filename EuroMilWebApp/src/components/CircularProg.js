import React from 'react';
import {CircularProgress} from "@material-ui/core";
import "../styles/circular-progress.scss";

const CircularProg = () => {
    return(
        <div className={"circular-wrapper"}>
            <CircularProgress />
        </div>
    );
};

export default CircularProg;
