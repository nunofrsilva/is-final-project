import React from 'react';
import {Link} from "react-router-dom";
import Button from '@material-ui/core/Button';

/**
 * Home Page
 * @returns {JSX.Element}
 * @constructor
 */
const HomeContent = () => {
    return (
        <div className={"home-content-wrapper"} >
            <Button variant="contained" color="primary" component={Link} to={'/bet'}>Registar Aposta</Button>
        </div>
    );
};

export default HomeContent;