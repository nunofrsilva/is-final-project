import React from 'react';
import {Link} from "react-router-dom";
import Button from '@material-ui/core/Button';
import {useApp} from "../overmind";
import {Container, Typography} from "@material-ui/core";
import CircularProg from "./CircularProg";
import '../styles/finish-content.scss';

/**
 * Finish page
 * @returns {JSX.Element}
 * @constructor
 */
const FinishContent = () => {
    const { state } = useApp();
    const {serverResponse} = state;
    let isLoaded = (serverResponse.euromil !== '');

    return (
        <div className={"finis-content-wrapper"}>
            {isLoaded ? (
                <div>
                    <Container className={"finish-wrapper"}>
                        <Typography variant="h5" component="h5" className={"title-text"}>
                            {serverResponse.euromil}
                        </Typography>
                    </Container>
                    <Container>
                        <Button variant="contained" color="primary" component={Link} to={'/bet'}>Registar Nova Aposta</Button>
                    </Container>
                </div>
            ):(<CircularProg />)}
        </div>
    );
};

export default FinishContent;