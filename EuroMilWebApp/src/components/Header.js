import React, {useState} from "react";
import {AppBar, Grid, IconButton, Menu, MenuItem, Toolbar, Typography} from "@material-ui/core";
import '../styles/header.scss'
import {useApp} from "../overmind";
import Icon from '@material-ui/core/Icon';

const userNames = [
    {key: "anonymous", value:"Anónimo"},
    {key: "teenagers", value:"Jovem"},
    {key: "adult", value:"Adulto"},
    {key: "senior", value:"Sénior"}
];

const Header = () => {
    const [anchorEl, setAnchorEl] = useState(null);
    const {state, actions} = useApp();
    const open = Boolean(anchorEl);
    const {userType} = state;
    const username = userNames.find(d => d.key === userType);
    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleSelectUser = (userType) => (event) => {
        actions.changeUserType(userType);
        setAnchorEl(null);
    };

    return (
        <div className={"header-wrapper"}>
            <Typography variant="h1" component="h2">
                EUROMIL
            </Typography>
            <div className={"stars"}>
                <Icon fontSize={"large"}>star</Icon>
                <Icon fontSize={"large"}>star</Icon>
                <Icon fontSize={"large"}>star</Icon>
                <Icon fontSize={"large"}>star</Icon>
                <Icon fontSize={"large"}>star</Icon>
            </div>
        </div>
    );
};

export default Header;