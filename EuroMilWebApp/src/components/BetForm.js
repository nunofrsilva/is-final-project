import React, {Component, useState} from "react";
import Button from "@material-ui/core/Button";
import Snackbar from '@material-ui/core/Snackbar';
import {Container, FormControl, Grid, TextField, Typography} from "@material-ui/core";
import '../styles/bet-content.scss';
import {Alert} from "@material-ui/lab";
import {useApp} from "../overmind";
import {useHistory} from "react-router-dom";

/**
 * This forms is responsible to handle with bet request
 */
const BetForm = () => {
    const { state, actions } = useApp();
    const {numbers, account, errorMessage} = state;
    const [open, setOpen] = React.useState(false);
    const history = useHistory();

    // Handle with numbers
    const handleChange = (event) => {
        const key = event.target.name;
        let value = event.target.value;
        let numbs = [...numbers];
        console.log(key);
        console.log(value);
        if ((/^[0-9]+$/.test(value) && parseInt(value) > 0 && parseInt(value) < 51) || value === '') {
            numbs[key] = value;
        }
        actions.changeNumbers(numbs);
    }
    // Handle with account number
    const handleAccount = (event) => {
        let value = event.target.value;
        console.log(value)
        if ((/^[0-9]+$/.test(value) || value === '') && value.length < 9) {
            //account = value
            actions.changeAccount(value)
        }
    }

    // Handle with snackbar close event
    const handleClose = (event) => {
        setOpen(false);
    }

    // handle with submission form
    const submit = (event)  => {
        if (!numbers.every((n) => /^[0-9]+$/.test(n)) || hasDuplicates(numbers)) {
            actions.setErrorMessage('Os números inseridos são inválidos! Não poderá conter números duplicados.');
            setOpen(true);
            return
        } else if (!/^[0-9]+$/.test(account) || account === '' || account.length !== 8) {
            actions.setErrorMessage('Número de conta CredBank invalido!');
            setOpen(true);
            return
        }
        actions.betRegister();
        history.push("/finish");
    }

    /**
     * Check if exists duplicate numbers
     * @param data
     * @returns {boolean}
     */
    const hasDuplicates = (data = []) => {
        const dups = new Set(data);
        return data.length !== dups.size;
    }

    // Render form

    return (
        <div className={"bet-wrapper"} >
            <Container className={"numbers-wrapper"}>
                <Typography variant="h5" component="h5" className={"title-text"}>
                    Insira 7 números entre 1 e 50
                </Typography>
                <Grid container spacing={1} justify={"center"}>
                    {numbers.map((data, index ) => {
                        return(
                            <Grid item key={index}>
                                <FormControl className={"input-field"}>
                                    <TextField onChange={handleChange} id={'number_'+index} name={''+index} variant="filled" value={data}/>
                                </FormControl>
                            </Grid>
                        );
                    })}
                </Grid>
            </Container>
            <Container className={"account-wrapper"}>
                <Typography variant="h5" component="h5" className={"title-text"}>
                    Insira o número da sua conta CredBank.
                </Typography>
                <FormControl>
                    <TextField onChange={handleAccount} id={"credbanckAccount"} name={"credbanckAccount"} variant="filled" value={account} />
                </FormControl>
            </Container>
            <div className={"submit-button-wrapper"}>
                <Button variant="contained" color="primary" onClick={submit}>Registar Aposta de 10 Créditos</Button>
            </div>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error">
                    {errorMessage}
                </Alert>
            </Snackbar>
        </div>
    );


};

export default BetForm;